package java8;

public interface DefinedInterface1 {
	void show();
	default void define(){
		System.out.println("Defined Interface Method");
	}
}
