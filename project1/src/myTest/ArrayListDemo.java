package myTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;


public class ArrayListDemo {
	public static void main(String args[]){
		ArrayList<Integer> arrays=new ArrayList<Integer>(Collections.nCopies(10, 3));
		System.out.println(arrays);
		arrays.trimToSize();
		System.out.println(arrays.size());
		ArrayList<String> arrayString= new ArrayList<String>();
		arrayString.add("A");
		arrayString.add("C");
		arrayString.add("B");
		arrayString.add("D");
		arrayString.add("E");
		arrayString.add("F");
		arrayString.add("G");
		arrayString.add("H");
		arrayString.add("I");
		for(String i:arrayString){
			 System.out.println(i);
		}
		//ENUMERATION ITERATION ARRAY
		System.out.println("Enumeration Iterate Array");
		Enumeration<String> e=Collections.enumeration(arrayString);
		
		while(e.hasMoreElements()){
			System.out.println(e.nextElement());
		}
		
		//SORT AN ARRAY ASSENDING
		Collections.sort(arrayString);
		System.out.println("Sorted Array is :"+arrayString);
		
		//SORT AN ARRAY IN DESCENDING
		Collections.reverse(arrayString);
		System.out.println("Reverse Order of Array"+arrayString);
		Collections.sort(arrayString, Collections.reverseOrder());
		System.out.println("Descending Order Array"+arrayString);
		System.out.println('A');
		
	}
	
}
