package myTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class ComparableTest {
	public static void main(String args[]) {
		List<Laptop> laps=new ArrayList<Laptop>();
		laps.add(new Laptop("Lenovo",1000,45000));
		laps.add(new Laptop("Dell",2000,50000));
		laps.add(new Laptop("Toshiba",3000,35000));
		laps.add(new Laptop("Samsung",4000,25000));
		System.out.println("Before Sort");
		for(Laptop laptop:laps){
			System.out.println(laptop);
		}
		Collections.sort(laps);
		System.out.println("\nAfter Sort By Ram");
		for(Laptop laptop:laps){
			System.out.println(laptop);
		}
	}
}
