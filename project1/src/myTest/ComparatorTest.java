package myTest;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class ComparatorTest {
	public static void Main(String args[]) {
		List<Mobile> mobileList = new ArrayList<Mobile>();
		mobileList.add(new Mobile("Moto", 10000, 5));
		mobileList.add(new Mobile("Samsung", 30000, 3));
		mobileList.add(new Mobile("Lenovo", 50000, 6));
		mobileList.add(new Mobile("Redmi", 40000, 4));

		System.out.println("Before Sort\n");
		for(Mobile mobile:mobileList){
			System.out.println(mobile);
		}
		Comparator<Mobile> comp = new Comparator<Mobile>() {
			@Override
			public int compare(Mobile o1, Mobile o2) {
				if (o1.getPrice() > o2.getPrice()) {
					return 1;
				} else {
					return -1;
				}
			}
		};
		Collections.sort(mobileList, comp);
		System.out.println("\nAfter Sort By Price\n");
		for(Mobile mobile:mobileList){
			System.out.println(mobile);
		}
	}
}
