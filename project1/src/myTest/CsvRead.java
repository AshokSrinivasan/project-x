package myTest;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

public class CsvRead {
	public static void main(String args[]) {
		Scanner scanner =new Scanner(System.in);
		System.out.println("Enter The Query");
		String query=scanner.nextLine().toUpperCase();
		if(query.contains("FROM")){
			String querySplit[]=query.split("FROM");
			String fileName=querySplit[1].trim().replaceAll("[;]","");;
			System.out.println(fileName);
		}
		scanner.close();
		CsvRead csvRead=new CsvRead();
		csvRead.getCsvList();
	}
	
	private void getCsvList(){
		String csvFile = "G:/testjavafile/ABCD.csv";
		String column[]={"A","B","C"};
		
        String line = "";
        String cvsSplitBy = ",";

        try (BufferedReader br = new BufferedReader(new FileReader(csvFile))) {
            while ((line = br.readLine()) != null) {
                // use comma as separator
                String[] country = line.split(cvsSplitBy);
                System.out.println(country[1] +"      "+ country[2]);
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

	}
}

