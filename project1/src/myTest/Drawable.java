package myTest;

@FunctionalInterface
public interface Drawable {
	void draw();
}
